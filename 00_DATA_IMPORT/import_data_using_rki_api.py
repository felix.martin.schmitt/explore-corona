import requests, datetime, argparse
import pandas as pd
import numpy as np

class data_import():
    
    def __init__(self,landkreis):
        self.landkreis=landkreis

    def func(self,df):
        return "{}-{}".format(str(df["AnzahlFall"]),str(df["AnzahlTodesfall"]))

    def reformat_whole_dataframe(self,data):
        data["data"]=data.apply(lambda x: self.func(x),axis=1)
        if self.landkreis:
            data=data[["Meldedatum","Landkreis","data"]]
            data=pd.pivot_table(data,index=["Meldedatum"],columns=["Landkreis"],aggfunc=lambda x: "".join(str(v) for v in x))
        else:
            data=data[["Meldedatum","Bundesland","data"]]
            data=pd.pivot_table(data,index=["Meldedatum"],columns=["Bundesland"],aggfunc=lambda x: "".join(str(v) for v in x))
        data=data.droplevel(None,axis=1).reset_index()
        data.columns=[col.lower() for col in data.columns]
        return data

    def reformat_features(self,data):
        data=data[["AnzahlFall","AnzahlTodesfall","Landkreis","Meldedatum"]] if self.landkreis else data[["AnzahlFall","AnzahlTodesfall","Bundesland", "Meldedatum"]] ##only keep needed columns
        
        l=[]
        for col in data.columns: ##go through all columns
            l.append(data[data[col]=="-nicht erhoben-"]) ##append all rows which contain a "-nicht erhoben-"
        data=data.drop(pd.concat(l).index,axis=0).reset_index(drop=True) if len(l)>0 else data ##drop all rows which contain a "-nicht erhoben-"
        data["Meldedatum"] = data["Meldedatum"].apply(lambda x: datetime.datetime.strftime(datetime.datetime.strptime(x,"%Y-%m-%dT%H:%M:%S.%fZ"),"%Y-%m-%d"))

        if self.landkreis: ##if landkreise shall be kept
            pivot_data=pd.pivot_table(data,index=["Meldedatum","Landkreis"], aggfunc=np.sum).reset_index() ##make pivot with all "AnzahlFall/AnzahlTodesfall" per day
            return pd.pivot_table(pivot_data.set_index(["Landkreis","Meldedatum"]),index="Landkreis",aggfunc=np.cumsum).reset_index().sort_values(by = "Meldedatum").reset_index(drop=True) ##cumsum over the days
        
        ##otherwise
        pivot_data=pd.pivot_table(data,index=["Meldedatum","Bundesland"], aggfunc=np.sum).reset_index() ##make pivot with all "AnzahlFall/AnzahlTodesfall" per day
        return pd.pivot_table(pivot_data.set_index(["Bundesland","Meldedatum"]),index="Bundesland",aggfunc=np.cumsum).reset_index().sort_values(by = "Meldedatum").reset_index(drop=True) ##cumsum over the days

    def api_data_import(self):
        url = "https://opendata.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0.geojson" ##url of api
        rawdata = requests.get(url).json()["features"] ##get json with data, returns a list
        listOfFrames=[]
        for d in rawdata: ##go through list
            listOfFrames.append(pd.DataFrame.from_dict(d["properties"],orient="index").T) ##add transformed DataFrame to new list
        data=pd.concat(listOfFrames).reset_index(drop=True) ##concat list of DataFrames and reset_index        
        data = self.reformat_features(data)
        
        data=self.reformat_whole_dataframe(data) ##reformat in new format that matches "virus.csv"
        return data
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser() ##init ArgumentParser
    parser.add_argument('--landkreis', type=bool, default=False, help='auf Landkreis- (=True) oder Bundesland (=False=default)-Ebene?') ##add argument
    args = parser.parse_args()
    
    di=data_import(args.landkreis) ##init class
    data=di.api_data_import() ##start parsing
    filename="parsed_api_data_bl.csv"
    if args.landkreis:
        filename="parsed_api_data_lk.csv"
    data.to_csv(filename,index=False) ##save