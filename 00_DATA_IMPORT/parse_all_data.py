from import_data_using_CSSE_api import data_import as csse_i
from import_data_using_RKI_api import data_import as rki_i
import datetime

if __name__ == "__main__":
    csse=csse_i()
    rki_bl=rki_i(False)
    rki_lk=rki_i(True)
    
    csse_data = csse.api_data_import()
    rki_bl_data = rki_bl.api_data_import()
    rki_lk_data = rki_lk.api_data_import()
    
    data=csse_data.merge(rki_bl_data,on="meldedatum").merge(rki_lk_data,on="meldedatum")
    data.to_csv("whole_parsed_data.csv",index=False) ##save
    
    file="../static/assets/timestamp.txt" ##access timestamp file
    timestamp = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S.%f") ##calc current timestamp in string format

    with open(file, "w") as f: ##open file
        text=f.write("timestamp\n{}".format(timestamp)) ##overwrite old timestamp