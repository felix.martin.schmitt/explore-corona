import requests,datetime
import pandas as pd

class data_import():
    
    def __init__(self):
        pass

    def func(self,df):
        return "{}-{}-{}".format(str(df["AnzahlFall"]),str(df["AnzahlGeheilt"]),str(df["AnzahlTodesfall"]))

    def reformat_whole_dataframe(self,data):
        data["data"]=data.apply(lambda x: self.func(x),axis=1)
        data=data[["Meldedatum","Land","data"]]
        data=pd.pivot_table(data,index=["Meldedatum"],columns=["Land"],aggfunc=lambda x: "".join(str(v) for v in x))
        data=data.droplevel(None,axis=1).reset_index()
        data.columns=[col.lower() for col in data.columns]
        return data
    
    def reformat_features(self,data):
        data.columns=["Meldedatum","AnzahlFall","AnzahlGeheilt","AnzahlTodesfall","Land"] ##reset column names
        data["Meldedatum"]=data["Meldedatum"].apply(lambda x: datetime.datetime.strftime(datetime.datetime.strptime(x,"%m/%d/%y"),"%Y-%m-%d")) ##reformat timestamp format
        data.sort_values(by="Meldedatum").reset_index(drop=True,inplace=True) ##sort by timestamp
        return data
    
    def api_data_import(self):
        url="https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/timeseries?onlyCountries=true" ##url of api
        rawdata = requests.get(url).json() ##get json with data, returns a list
        listOfFrames=[]
        for d in rawdata: ##go through list of countries
            tempframe=pd.DataFrame.from_dict(d["timeseries"],orient="index") ##add data
            tempframe["Land"]=d["countryregion"] ##add country
            listOfFrames.append(tempframe) ##append to list
        data=pd.concat(listOfFrames).reset_index() ##concat all snippets
        data=self.reformat_features(data) ##reformat some data
        
        data=self.reformat_whole_dataframe(data) ##reformat in new format that matches "virus.csv"
        return data
    
if __name__ == "__main__":
    
    di=data_import() ##init class
    data=di.api_data_import() ##start parsing
    filename="parsed_api_data_world.csv"
    data.to_csv(filename,index=False) ##save