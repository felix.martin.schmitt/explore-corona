"""
A simple flask server
"""
import atexit
import os
import time
from os import environ as env
import datetime


from flask import (Flask, abort, jsonify, request,
                   send_from_directory)
from flask_cors import CORS

from apscheduler.schedulers.background import BackgroundScheduler
from case_prediction import update_timeseries_plot_callback


app = Flask(__name__, static_url_path='')

app.config.from_object(env['APP_SETTINGS'])
CORS(app)

def download_data():
  os.system('python 00_DATA_IMPORT/import_data_using_rki_api.py')
  os.system('python 00_DATA_IMPORT/import_data_using_rki_api.py')

# create schedule for printing time
scheduler = BackgroundScheduler()
scheduler.add_job(download_data, 'interval', days=1)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/predictions/<country>')
def predictions(country):
    data = update_timeseries_plot_callback(country)
    print(data)
    return jsonify({"status":"success", "country": country,"data": data})


if __name__ == '__main__':
    app.run()