# CovInteractive
This repository stores the source code for the [ConInteractive Website] (https://covinteractive.herokuapp.com/), a contribution to the [WeVsVirus Hackathon](https://wirvsvirushackathon.org/). This online interactive map enables users to track both the global and local trends of Novel Coronavirus infection since Jan 21st, 2020. The dataset is timely collected from multiple official sources and then plotted onto this map.

## Inspiration
Ein großes Problem ist, dass die Gefahr durch COVID-19 unsichtbar ist und diszipliniertes Handeln jetzt in den nächsten Wochen viele Menschenleben retten kann. Um dies anschaulicher zu machen, wollen wir eine interaktive Website anbieten, die über die momentanen Daten hinaus auch verschiedene Vorhersagen visualisiert und so die positiven Konsequenzen sozialer Isolation veranschaulicht.

## Technische Umsetzung
Wir bauen auf einem bestehendem Projekt des [Humanistic GIS Lab](https://hgis.uw.edu/virus/) auf und erweitern sie um folgende Features:
- Vorhersagen nach [Susceptible-Infected-Removed-Modell](https://de.wikipedia.org/wiki/SIR-Modell) (SIR-Modell)
- Einbindung von Daten aus Deutschland auf Bundesland- und Landkreisebene vom [Robert Koch-Institut](https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html)
- Einbindung weltweiter Daten vom [Johns Hopkins Institute](https://github.com/CSSEGISandData/COVID-19)

## Weitere Entwicklung
- Ein Merge zurück in das Originalrepo ist für nach dem Hackaton geplant, nachdem alle Bugs erfolgreich ausgeschaltet wurden
- Darstellung relativer Zahlen in der Weltkarte
- Übersetzung in weitere Sprachen (französisch, italienisch, türkisch...)
- Finetuning der Modellierung

## Größte Herausforderungen
- Die Implementierung des ersten Prototypen, sodass alle eine gemeinsame Arbeitsbasis hatten
- Zeitmangel
- Modellierung der Infektionsverläufe
- Datenaufbereitung
- git ;-)

## Acknowledgement:
- Team members: .
- This project is based on [the Novel Corona Virus Infection Map](https://github.com/jakobzhao/virus) from the
 the [Humanistic GIS Lab](https://hgis.uw.edu) at [University of Washington – Seattle](https://www.uw.edu). Thank you for allowing us to use your code!

