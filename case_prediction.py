#!/usr/bin/python3
''' prediction of future case numbers based on the SIR-modell
https://de.wikipedia.org/wiki/SIR-Modell

v2.0'''


from functools import partial
from collections import defaultdict
from datetime import datetime

import numpy as np
import pandas as pd
from scipy import optimize
from scipy import integrate

import matplotlib.pyplot as plt




model_sir_n_params = 2
model_sir_n_eq = 3
def model_sir_ode(_t, y, model_params):
    """
    Represents the rhs of the system of ODEs of the SIR model.
    """
    S, I, R = y
    N = S + I + R
    beta, gamma = model_params
    
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    
    return dSdt, dIdt, dRdt


__cache_solution_for = {}
__cached_solution = {}
def numeric_model_funcs(model_ode, t_span, model_params, init_vals):
    """
    Return a function of time, which is a solution of the given initial value problem. Uses a simple cache for the last solved ivp per model.
    @param model_ode: The rhs of a system of ODEs.
    @param t_span: The time span, over which the system shall be integrated. Defines the times on which the return value is defined.
    @param model_params: A sequence of values for the parameters of the given model.
    @param init_vals: A sequence of initial values for the ivp to be solved.
    @return: A function of time, whose values are containers bundling the values of all unknowns of the ivp at the given time.
    """
    cache_marker = (t_span, model_params, init_vals)
    if model_ode in __cache_solution_for and __cache_solution_for[model_ode] == cache_marker:
        return __cached_solution[model_ode]
    else:
        ivp_solution = integrate.solve_ivp(partial(model_ode, model_params=model_params),
                                           t_span,
                                           init_vals,
                                           dense_output=True).sol
        __cache_solution_for[model_ode] = cache_marker
        __cached_solution[model_ode] = ivp_solution
        return ivp_solution

def fit_model_to_data(model_ode, n_model_params, ts, ys, i=0, t_span=None, fit_param_guess=None, bounds=None):
    """
    Fit the given ODE model to data, determining both the optimal model parameters and initial conditions.
    @param model_ode: A function representing the rhs of a system of ODEs.
    @param n_model_params: Number of parameters of the given model. Serves to disentangle parameters and initial conditions.
    @param ts: A sequence of data time values.
    @param ys: A sequence of data function values.
    @param i: Selects the function of the ODE model to be fitted to data. By default, selects the first function.
    @param t_span: Time span, over which the ODE model shall be integrated. By default, the minimum range covering all ts is chosen.
    @param bounds: Pair of sequences defining lower/upper bounds for the fit parameters. Defaults to no bounds.
    @param fit_param_guess: A sequence of guesses for both model parameters and initial values. Used as the starting point of the fit.
    @return: Sequence of fit results for model parameters, sequence of fit results for initial values
    """
    if t_span is None:
        t_span = (min(ts), max(ts))
    popt, _ = optimize.curve_fit(lambda t, *args: numeric_model_funcs(model_ode, t_span, args[:n_model_params], args[n_model_params:])(t)[i],
                                 ts, ys,
                                 p0=fit_param_guess,
                                 bounds=bounds)
    return popt[:n_model_params], popt[n_model_params:]


def compute_model_value_table(model_ode, model_params, init_vals, ts, t_span=None, i=None):
    """
    For the given ODE model and its parameters and initial values, compute a table of values.
    @param model_ode: A function representing the rhs of a system of ODEs.
    @param model_params: A sequence of values for the parameters of the given model, to be used in the computation.
    @param init_vals: A sequence of initial values for the model ivp, to be used in the computation.
    @param ts: A sequence of time values at which the model function shall be evaluated.
    @param t_span: The time span, for which the model ivp shall be solved. Defaults to the minimum range containing all ts.
    @param i: Selects one of the model functions to be used for the value table. Defaults to all functions.
    @return: Sequence of pairs with time value and model function value(s)
    """
    if t_span is None:
        t_span = (min(ts), max(ts))
    model_funcs = numeric_model_funcs(model_ode, t_span, model_params, init_vals)
    if i is None:
        return np.array([model_funcs(t) for t in ts])
    else:
        return np.array([model_funcs(t)[i] for t in ts])


def fit_sir_to_infection_data(ts, ys, t_span=None, bounds=None, param_guess=None):
    """
    Fit the SIR model to infection data I(t).
    @param ts: A sequence of time values.
    @param ys: A sequence of infection data values.
    @param t_span: Time span, over which the ODE model shall be integrated. By default, the minimum range covering all ts is chosen.
    @param bounds: Pair of sequences defining lower/upper bounds for the fit parameters. Defaults to no bounds.
    @param param_guess: A sequence of guesses for beta, gamma, S0, I0 and R0. Used as the starting point of the fit.
    @return: (beta, gamma), (S0, I0, R0)
    """
    return fit_model_to_data(model_sir_ode, model_sir_n_params,
                             ts, ys,
                             i=1,
                             t_span=t_span, bounds=bounds, fit_param_guess=param_guess)


def compute_sir_value_table(ts, beta, gamma, S0, I0, R0, i=None):
    return compute_model_value_table(model_sir_ode, (beta, gamma), (S0, I0, R0), ts, i=i)


def parse_virus_csv_cell(content: str):
    if content == 'nan':
        return (0,)*3
    else:
        numbers = content.split(sep='-')
        while len(numbers) < 4:
            numbers.append('0')
        return tuple(map(lambda i: int(numbers[i]), [0, 2, 3]))


def update_timeseries_plot_callback(region_name: str):
    data_path = './static/assets/virus.csv'
    data = pd.read_csv(data_path)

    populations = pd.read_csv('./static/assets/worldpopulation.csv')
    n = int(populations[region_name.lower()][0])

    time_format = '%Y-%m-%d'
    t = np.array(list(map(lambda x: datetime.strptime(x, time_format).toordinal(), data['datetime'])))
    region_data = np.array(data[region_name], dtype=str)
    data = np.array(list(map(parse_virus_csv_cell, region_data)))

    total_confirmed, total_recovered, total_deaths = data.T
    r = total_recovered + total_deaths
    i = total_confirmed - r
    s = n - r - i

    prediction_horizon = 100
    t_pred = np.arange(0, prediction_horizon) + t[-1]
    s_pred, i_pred, r_pred = compute_sir_value_table(t_pred, beta=0.1, gamma=0.1, S0=s[-1], I0=i[-1], R0=r[-1]).T

    letality = total_deaths[-1] / total_confirmed[-1]
    total_confirmed_pred = r_pred + i_pred
    total_deaths_pred = letality * total_confirmed_pred
    total_recovered_pred = total_confirmed_pred - i_pred - total_deaths_pred

    # plt.plot(t, total_confirmed, 'ro-')
    # plt.plot(t, i, 'o-', color='orange')
    # plt.plot(t, total_recovered, 'og-')
    # plt.plot(t, total_deaths, 'bo-')
    #
    # plt.plot(t_pred, total_confirmed_pred, 'r', dashes=[6, 3])
    # plt.plot(t_pred, i_pred, color='orange', dashes=[6, 3])
    # plt.plot(t_pred, total_recovered_pred, 'g', dashes=[6, 3])
    # plt.plot(t_pred, total_deaths_pred, 'b', dashes=[6, 3])
    #
    # plt.show()


    t_out = ['x'] + [datetime.fromordinal(date).strftime(time_format) for date in np.hstack([t, t_pred]).tolist()]
    total_confirmed = ['confirmed'] + total_confirmed.tolist() + ['null'] * prediction_horizon
    active_confirmed = ['active'] + i.tolist() + ['null'] * prediction_horizon
    total_recovered = ['recovered'] + total_recovered.tolist() + ['null'] * prediction_horizon
    total_deaths = ['dead'] + total_deaths.tolist() + ['null'] * prediction_horizon
    total_confirmed_pred = ['confirmed_pred'] + ['null']*len(t) + total_confirmed_pred.tolist()
    active_confirmed_pred = ['active_pred'] + ['null']*len(t) + i.tolist()
    total_recovered_pred = ['recovered_pred'] + ['null']*len(t) + total_recovered_pred.tolist()
    total_deaths_pred = ['dead_pred'] + ['null']*len(t) + total_deaths_pred.tolist()

    return [t_out, total_confirmed, active_confirmed, total_recovered, total_deaths,
            total_confirmed_pred, active_confirmed_pred, total_recovered_pred, total_deaths_pred]


if __name__ == '__main__':
    bl_data = defaultdict(list)
    ret = update_timeseries_plot_callback('hubei')
    pass




